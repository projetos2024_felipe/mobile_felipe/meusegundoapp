import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function LayoutGrade() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.linha}>
          <View style={styles.box1}></View>
          <View style={styles.box2}></View>
        </View>
        <View style={styles.linha}>
          <View style={styles.box3}></View>
          <View style={styles.box4}></View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  linha: {
    flexDirection: "row",
  },
  box1: {
    width: 50,
    height: 50,
    backgroundColor: "pink",
  },
  box2: {
    width: 50,
    height: 50,
    backgroundColor: "brown",
  },
  box3: {
    width: 50,
    height: 50,
    backgroundColor: "cyan",
  },
  box4: {
    width: 50,
    height: 50,
    backgroundColor: "purple",
  },
});
