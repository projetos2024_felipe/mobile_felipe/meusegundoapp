import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function Layout() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>MENU</Text>
      </View>
      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>  
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>
          <Text style={styles.text}>CONTEÚDO</Text>


        </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text style={styles.text}>RODAPÉ</Text>
      </View>

      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"column",
    backgroundColor: "#fff",
    justifyContent: "space-between",
    
  },
  header:{
    height: 50,
    backgroundColor: 'blue',
  },
  content:{
    flex: 1,
    backgroundColor: 'green',
  },
  footer:{
    height: 50,
    backgroundColor: 'red',
  },
  text:{
    fontSize:16,
    fontWeight:'bold',
    textAlign: 'center',
  },
}); 
